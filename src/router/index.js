import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/home.vue'
import curd from '../views/BaseCURD/index.vue'
import Layout from '../views/layout/index.vue'

const routes = [
  { 
    path: '/',
    component: Layout,
    redirect: '/index',
  },
  { 
    name:'index',
    path: '/index', 
    component: Home,
  },
  {
    name:'curd',
    path: '/curd', 
    component: curd,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})

export default router