import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from './router'
import VueClipboard from 'vue-clipboard2'

const app = createApp(App)
app.use(router)
app.use(ElementPlus)
app.use(VueClipboard)
app.mount('#app')